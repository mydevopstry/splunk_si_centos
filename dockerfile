# Используйте базовый образ
FROM centos

RUN \
    cd /etc/yum.repos.d/ && \
    sed -i 's/mirrorlist/#mirrorlist/g' CentOS-* && \
    sed -i 's|#baseurl=http://mirror.centos.org|baseurl=http://vault.centos.org|g' CentOS-*


# Установите необходимые пакеты и зависимости
RUN yum install -y sudo
