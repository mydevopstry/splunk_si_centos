#!/bin/bash


sudo yum install -y ntp
sudo timedatectl set-timezone Asia/Ulaanbaatar 
sudo sh -c 'echo "server time.google.com iburst" >> /etc/ntp.conf'
sudo systemctl enable ntpd  
sudo systemctl start ntpd   
sudo timedatectl set-ntp true



# Редактирование sysctl.conf
echo "fs.file-max = 65536" | sudo tee -a /etc/sysctl.conf


# Редактирование limits.conf
echo "splunk    soft    nofile    65535" | sudo tee -a /etc/security/limits.conf
echo "splunk    hard   nofile    65535" | sudo tee -a /etc/security/limits.conf
echo "splunk    soft    nproc    20480" | sudo tee -a /etc/security/limits.conf
echo "splunk    hard   nproc    20480" | sudo tee -a /etc/security/limits.conf
echo "splunk    hard   core    unlimited" | sudo tee -a /etc/security/limits.conf

# Редактирование 90-nproc.conf
echo "splunk    soft    nofile    65535" | sudo tee -a /etc/security/limits.d/90-nproc.conf
echo "splunk    hard   nofile    65535" | sudo tee -a /etc/security/limits.d/90-nproc.conf
echo "splunk    soft    nproc    20480" | sudo tee -a /etc/security/limits.d/90-nproc.conf
echo "splunk    hard   nproc    20480" | sudo tee -a /etc/security/limits.d/90-nproc.conf

# Создание директории и файла для systemd
sudo mkdir -p /etc/systemd/system/splunk.service.d/
echo "[Service]" | sudo tee /etc/systemd/system/splunk.service.d/filelimit.conf
echo "LimitNOFILE=65535" | sudo tee -a /etc/systemd/system/splunk.service.d/filelimit.conf
sudo -i source ~/.bashrc


# Добавление портов в зону public
sudo firewall-cmd --zone=public --permanent --add-port=8000/tcp 
sudo firewall-cmd --zone=public --permanent --add-port=8089/tcp
sudo firewall-cmd --zone=public --permanent --add-port=9997/tcp 
sudo firewall-cmd --zone=public --permanent --add-port=9100/tcp 
sudo firewall-cmd --zone=public --permanent --add-port=9200/tcp 
sudo firewall-cmd --zone=public --permanent --add-port=9887/tcp 
sudo firewall-cmd --zone=public --permanent --add-port=443/tcp

# Проксирование порта 443 на порт 8000
sudo firewall-cmd --add-forward-port=port=443:proto=tcp:toport=8000


# Перезагрузка файрвола
sudo firewall-cmd --reload

# Сохранение правил перманентно
sudo firewall-cmd --runtime-to-permanent



#Disable THP
sudo tee /etc/systemd/system/disable-transparent-huge-pages.service > /dev/null <<EOF
[Unit]
Description=Disable Transparent Huge Pages

[Service]
Type=oneshot
ExecStart=/bin/sh -c "echo never > /sys/kernel/mm/transparent_hugepage/enabled"
ExecStart=/bin/sh -c "echo never > /sys/kernel/mm/transparent_hugepage/defrag"
RemainAfterExit=true

[Install]
WantedBy=multi-user.target
EOF




# Включение и запуск службы отключения Transparent Huge Pages (THP)
sudo systemctl enable disable-transparent-huge-pages.service 
sudo systemctl start disable-transparent-huge-pages.service 




sudo useradd -r splunk
sudo id splunk
sudo passwd splunk <<EOF
Splunk123
Splunk123
EOF
echo "splunk    ALL=(ALL)       ALL" | sudo tee -a /etc/sudoers
sudo mkdir /home/splunk
sudo chown -R splunk:splunk /home/splunk/
sudo usermod -d /home/splunk splunk

sudo yum install -y wget
sudo wget -O splunk-9.1.0.2-b6436b649711-Linux-x86_64.tgz "https://download.splunk.com/products/splunk/releases/9.1.0.2/linux/splunk-9.1.0.2-b6436b649711-Linux-x86_64.tgz"
sudo tar xvzf splunk-9.1.0.2-b6436b649711-Linux-x86_64.tgz -C /opt/
sudo chown -R splunk:splunk /opt/splunk
sudo -u splunk /opt/splunk/bin/splunk start --no-prompt --accept-license
sudo -u splunk tee /opt/splunk/etc/system/local/user-seed.conf > /dev/null <<EOF
[user_info]
USERNAME = splunk
PASSWORD = 123qweasd
EOF
sudo /opt/splunk/bin/splunk enable boot-start -user splunk
sudo -u splunk /opt/splunk/bin/splunk restart
